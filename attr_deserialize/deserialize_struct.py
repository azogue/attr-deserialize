"""
Methods to de-serialize complex attr dataclasses
from their raw dict representation
with the help of the typing info included in their definitions.
"""
from enum import Enum
import typing
from typing import Mapping, Sequence, Type, Union

from anytree import AnyNode
import attr

# Response values to be parsed as None:
NULL_VALUES = ("Not_implemented",)


class _FieldKind(Enum):
    """Different types of fields to parse."""

    STANDARD = 0
    SEQUENCE = 1
    DICT = 2
    UNION = 3
    DATACLASS = 4
    ENUM = 5
    TREE = 6
    NONE = 7
    ANY = 8


def _get_field_kind(field_type) -> _FieldKind:
    """Extract kind of item to select deserialize logic to apply to it."""
    # noinspection PyProtectedMember,PyUnresolvedReferences
    if isinstance(field_type, typing._GenericAlias):
        # noinspection PyProtectedMember
        if field_type._inst:
            return _FieldKind.UNION
        elif len(field_type.__args__) == 1:
            return _FieldKind.SEQUENCE

        # noinspection PyProtectedMember
        assert field_type._name == "Dict"
        return _FieldKind.DICT

    elif hasattr(field_type, "__attrs_attrs__"):
        return _FieldKind.DATACLASS

    elif field_type == typing.VT or field_type == typing.T:
        return _FieldKind.ANY

    elif not isinstance(field_type, type) or (
        issubclass(field_type, None.__class__)
    ):
        return _FieldKind.NONE

    elif issubclass(field_type, Enum):
        return _FieldKind.ENUM

    elif issubclass(field_type, AnyNode):
        return _FieldKind.TREE

    assert isinstance(field_type, type)
    return _FieldKind.STANDARD


def _check_result(
    field_kind, field_type, value, strict: bool = False, key=None
) -> bool:
    """Check if value is of expected type and raises error if strict check."""
    if field_kind == _FieldKind.NONE:
        ok = value is None
    else:
        ok = isinstance(value, field_type)

    if strict and not ok:
        raise ValueError(
            f"WARNING: Bad parsing with '{value}' (Not a {field_type})! "
            f"key: '{key}'"
        )
    return ok


def _process_union(field_type_union, value):
    """Process all combinations of typing Union and reduces to one.

    So, when using Union[X, Y, Z], these X, Y, Z types have to be
    mutually exclusive between them.
    """
    possibles = []
    for p_f_type in field_type_union.__args__:
        try:
            possibles.append(_parse_field(p_f_type, value, strict=True))
        except ValueError:
            pass

    if len(possibles) != 1:
        raise ValueError(
            f"No unique solution found (#{len(possibles)} possibles) "
            f"for union '{field_type_union}' with value = {value}."
        )
    return possibles[0]


def _process_tree_node(
    field_type: Type[AnyNode], value, parent=None, strict=False
):
    """Process a tree object contained in some dataclass field."""
    node_data = {}
    for field, f_type in field_type.__annotations__.items():
        if field in value:
            node_data[field] = _parse_field(
                f_type, value[field], strict=strict
            )

    node = field_type(parent=parent, **node_data)
    if "children" in value:
        for child_value in value["children"]:
            _process_tree_node(
                field_type, child_value, parent=node, strict=strict
            )
    return node


def _parse_field(field_type, value, strict=False, key=None):
    """Process simple field content."""
    field_kind = _get_field_kind(field_type)
    if field_kind == _FieldKind.ANY:
        return value
    elif field_kind in [_FieldKind.STANDARD, _FieldKind.ENUM, _FieldKind.NONE]:
        parsed = value
        if field_kind == _FieldKind.ENUM:
            parsed = field_type(value)
        _check_result(field_kind, field_type, parsed, strict=strict, key=key)
        return parsed
    elif field_kind == _FieldKind.DATACLASS and isinstance(value, Mapping):
        return deserialize_struct(field_type, value, strict=strict)
    elif field_kind == _FieldKind.TREE and isinstance(value, Mapping):
        return _process_tree_node(field_type, value, strict=strict)
    elif field_kind == _FieldKind.UNION:
        return _process_union(field_type, value)
    elif field_kind == _FieldKind.SEQUENCE and isinstance(value, Sequence):
        f_type_u = field_type.__args__[0]
        return [
            _parse_field(f_type_u, item, strict=strict, key=key)
            for item in value
        ]
    elif field_kind == _FieldKind.DICT and isinstance(value, Mapping):
        f_type_u = field_type.__args__[1]
        return {
            key: _parse_field(f_type_u, item, strict=strict, key=key)
            for key, item in value.items()
        }

    raise ValueError(
        f"WARNING: Bad parsing with '{value}' "
        f"(Not a {field_type} [{field_kind.name}])! key: '{key}'"
    )


def deserialize_struct(
    destination: Union[Type[attr.dataclass], Type[AnyNode]],
    raw_data: Mapping,
    null_values: Sequence = NULL_VALUES,
    strict: bool = True,
):
    """De-serialize a raw data dict in data-class (with nested data-classes).

    This de-serialization is made by exploring the typing info of the
    main data-class and walking the nested tree, trying all possible
    combinations while expecting only one good result.

    For this approach to work properly, when defining data-classes typing
    **must be detailed**, in order to restrain possibilities adequately.

    It also supports de-serializing a AnyNode tree object.

    This approach is able to deserialize complex combinations of nested data,
    like these:

    ```
    @attr.s
    class NestedItem:
        block_1: Union[PHexDisp, List[Union[PHexComponent, Filter, Fan]]]
        sup: List[Union[FanDisp, FilterDisp]]
        components: Dict[str, Union[PHexComponent, Filter, Fan]]
        panels: List[List[FilterPanelSize]]
    ```
    """
    field_kind = _get_field_kind(destination)
    if field_kind == _FieldKind.TREE:
        return _process_tree_node(destination, raw_data, strict=strict)

    assert field_kind == _FieldKind.DATACLASS
    field_names_types = {a.name: a.type for a in destination.__attrs_attrs__}

    parsed = {}
    for key, value in raw_data.items():
        if key not in field_names_types:
            raise ValueError(
                f"PARSING ERROR: '{key}' field [{type(value)}] "
                f"not in data-class: {destination}"
            )
        elif value in null_values:
            value = None

        parsed[key] = _parse_field(
            field_names_types[key], value, strict=strict, key=key
        )

    return destination(**parsed)
