# -*- coding: utf-8 -*-
"""pytest fixtures used in test collection."""
from pathlib import Path

import pytest
import yaml

TEST_PATH = Path(__file__).parent


@pytest.fixture
def testing_set():
    p_yml_results = TEST_PATH / "testing_set.yaml"
    with open(p_yml_results, "r", encoding="utf-8") as f:
        return yaml.load(f)
