from enum import IntEnum
from typing import Dict, List, Optional, Union, Tuple

import attr
import pytest
from attr_deserialize import deserialize_struct


class ExampleEnum(IntEnum):
    ONE = 1
    TWO = 2


@attr.s
class A:
    a1: int = attr.ib(default=0, validator=attr.validators.instance_of(int))
    a2: str = attr.ib(default="", validator=attr.validators.instance_of(str))


@attr.s
class B:
    a: A = attr.ib(validator=attr.validators.instance_of(A), factory=A)
    b1: Optional[int] = attr.ib(default=0)
    b2: str = attr.ib(default="", validator=attr.validators.instance_of(str))


@attr.s(auto_attribs=True)
class SimplerA:
    """Same as 'A' dataclass but without type validation."""

    a1: int = 0
    a2: str = ""


@attr.s(auto_attribs=True)
class SimplerB:
    """Same as 'B' dataclass but without type validation."""

    a: SimplerA = attr.ib(factory=SimplerA)
    b1: Optional[int] = 0
    b2: str = ""


@attr.s
class C:
    a: A = attr.ib(validator=attr.validators.instance_of(A), factory=A)
    e1: Optional[ExampleEnum] = attr.ib(default=None)
    e2: Union[str, ExampleEnum, A] = attr.ib(default=str)


@attr.s
class Complex:
    a: List[A] = attr.ib(factory=list)
    b: Dict[str, Union[A, B]] = attr.ib(factory=dict)
    c: Union[A, B] = attr.ib(factory=A)
    d: Tuple[Union[A, B]] = attr.ib(factory=list)
    e: List[List[Optional[B]]] = attr.ib(factory=list)
    f: Union[A, str, A] = attr.ib(factory=A)
    g: Union[Optional[dict], str] = attr.ib(factory=dict)
    h: Union[dict, str, None] = attr.ib(factory=str)
    i: ExampleEnum = attr.ib(default=ExampleEnum.ONE)
    j: Optional[C] = attr.ib(default=None)


def test_deserialize_success():
    raw_data1 = {"a": {"a1": 1, "a2": "str_a"}, "b1": 2, "b2": "str_b"}

    b = deserialize_struct(B, raw_data=raw_data1)
    assert b.a.a1 == raw_data1["a"]["a1"]
    assert b.a.a2 == raw_data1["a"]["a2"]
    assert b.b1 == raw_data1["b1"]
    assert b.b2 == raw_data1["b2"]


def test_deserialize_strict():
    raw_data_good = {"a": {"a1": 1, "a2": "str_a"}, "b1": 2, "b2": "str_b"}

    raw_data_bad_1 = {"a": {"a1": 1., "a2": "str_a"}, "b1": 2, "b2": 2}
    raw_data_bad_2 = {"a": {"a1": "1", "a2": "str_a"}, "b1": 2, "b2": "str_b"}
    raw_data_bad_3 = {"a": {"a1": 1, "a2": "str_a"}, "b1": 2, "b2": 3}

    b = deserialize_struct(SimplerB, raw_data_good)
    b_2 = deserialize_struct(SimplerB, raw_data_good, strict=True)
    assert isinstance(b.a, SimplerA)
    assert isinstance(b.a.a1, int)
    assert isinstance(b.a.a2, str)
    assert isinstance(b.b1, int)
    assert isinstance(b.b2, str)
    assert b == b_2

    # Not strict parsing (variable types are not equal to typing definition):
    b = deserialize_struct(SimplerB, raw_data_bad_1, strict=False)
    assert isinstance(b.a, SimplerA)
    with pytest.raises(AssertionError):
        assert isinstance(b.a.a1, int)
    assert isinstance(b.a.a1, float)

    b = deserialize_struct(SimplerB, raw_data_bad_2, strict=False)
    assert isinstance(b.a, SimplerA)
    with pytest.raises(AssertionError):
        assert isinstance(b.a.a1, int)
    assert isinstance(b.a.a1, str)

    # With/without strict parsing:
    deserialize_struct(SimplerB, raw_data_bad_1, strict=False)
    with pytest.raises(ValueError):
        deserialize_struct(SimplerB, raw_data_bad_1, strict=True)
    deserialize_struct(SimplerB, raw_data_bad_2, strict=False)
    with pytest.raises(ValueError):
        deserialize_struct(SimplerB, raw_data_bad_2, strict=True)
    deserialize_struct(SimplerB, raw_data_bad_3, strict=False)
    with pytest.raises(ValueError):
        deserialize_struct(SimplerB, raw_data_bad_3, strict=True)


def test_deserialize_nested():
    raw_data_a = {"a1": 1, "a2": "str_a"}
    raw_data_b = {"a": raw_data_a, "b1": 2, "b2": "str_b"}
    raw_data_c = {
        "a": [raw_data_a] * 3,
        "b": {"1": raw_data_b, "2": raw_data_b, "3": raw_data_a},
        "c": raw_data_b,
        "d": [raw_data_a, raw_data_b, raw_data_a],
        "e": [
            [raw_data_b, None, raw_data_b],
            [raw_data_b, raw_data_b, raw_data_b],
        ],
        "f": "raw_data_a",
        "g": "raw_data_a",
        "i": 2,
        "j": {"a": raw_data_a, "e1": 2, "e2": "str_b"},
    }

    complex_obj = deserialize_struct(Complex, raw_data=raw_data_c)
    assert complex_obj.a[2].a1 == raw_data_a["a1"]
    assert complex_obj.a[2].a2 == raw_data_a["a2"]
    assert complex_obj.b["1"].b1 == raw_data_b["b1"]
    assert complex_obj.b["1"].b2 == raw_data_b["b2"]
    assert isinstance(complex_obj.c, B)
    assert isinstance(complex_obj.d[0], A)
    assert isinstance(complex_obj.d[1], B)
    assert isinstance(complex_obj.d[2], A)
    assert complex_obj.e[0][1] is None
    assert isinstance(complex_obj.e[1][2], B)
    assert isinstance(complex_obj.f, str)
    assert isinstance(complex_obj.i, ExampleEnum)
    assert isinstance(complex_obj.j, C)
    assert isinstance(complex_obj.j.a, A)
    assert isinstance(complex_obj.j.e1, ExampleEnum)
    assert complex_obj.j.e1 is ExampleEnum.TWO
    assert isinstance(complex_obj.j.e2, str)
    assert isinstance(complex_obj.j, C)


def test_deserialize_null_values():
    raw_data1 = {"b1": "Not_Implemented"}

    b = deserialize_struct(
        B, raw_data=raw_data1, null_values=["Not_Implemented"]
    )
    assert b.b1 is None


def test_deserialize_fail():
    raw_data1 = {"b1": "some_data"}

    with pytest.raises(ValueError):
        deserialize_struct(B, raw_data=raw_data1)
