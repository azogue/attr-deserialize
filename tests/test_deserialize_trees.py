"""
Test of deserialization of dataclasses with AnyNode trees attached as
fields, these with dataclasses attached in their tree nodes.

class ExampleTree(AnyNode):
    quantity: int
    data: SomeAttrDataclass

@attr.s(auto_attribs=True)
class DataClassWithTrees:
    trees: Dict[str, ExampleTree] = attr.ib(factory=dict)

"""
import json
from typing import List, Optional, Union

import attr
from anytree import AnyNode, RenderTree
import pytest

from attr_deserialize import deserialize_struct, serialize_struct


@attr.s
class A(object):
    a1: int = attr.ib(default=0, validator=attr.validators.instance_of(int))
    a2: str = attr.ib(default="", validator=attr.validators.instance_of(str))


@attr.s
class B(object):
    a: A = attr.ib(validator=attr.validators.instance_of(A), factory=A)
    b1: Optional[int] = attr.ib(default=0)
    b2: str = attr.ib(default="", validator=attr.validators.instance_of(str))


class ExampleTree(AnyNode):
    quantity: int
    other: float
    data: B


@attr.s(auto_attribs=True)
class DataClassWithTrees:
    trees1: List[ExampleTree] = attr.ib(factory=list)
    trees2: Union[str, ExampleTree] = attr.ib(factory=str)
    trees3: Union[str, ExampleTree] = attr.ib(factory=str)


def test_serialize_tree(testing_set):
    raw_data_a = {"a1": 1, "a2": "str_a"}
    raw_data_b = {"a": raw_data_a, "b1": 2, "b2": "str_b"}
    root = ExampleTree(
        quantity=1, other=0.0, data=deserialize_struct(B, raw_data_b)
    )
    c1 = ExampleTree(
        quantity=2,
        other=1.0,
        data=deserialize_struct(B, raw_data_b),
        parent=root,
    )
    ExampleTree(
        quantity=3,
        other=2.0,
        data=deserialize_struct(B, raw_data_b),
        parent=root,
    )
    ExampleTree(
        quantity=4,
        other=3.0,
        data=deserialize_struct(B, raw_data_b),
        parent=c1,
    )

    print(RenderTree(root))
    data_tree = serialize_struct(root)
    assert data_tree == testing_set["example_tree_object"]

    new_root = deserialize_struct(ExampleTree, data_tree)
    assert isinstance(new_root, ExampleTree)
    assert isinstance(new_root.data, B)
    assert new_root.data == root.data
    assert isinstance(new_root.children[1], ExampleTree)
    assert isinstance(new_root.children[1].data, B)
    assert new_root.children[1].data == root.children[1].data
    assert isinstance(new_root.children[0].children[0], ExampleTree)
    assert isinstance(new_root.children[0].children[0].data, B)
    assert (
        new_root.children[0].children[0].data
        == root.children[0].children[0].data
    )

    # AnyNode objects don't have a __eq__ method to compare attributes
    assert deserialize_struct(ExampleTree, data_tree) != root


def test_serialization_of_struct_with_trees(testing_set):
    data_tree = testing_set["example_tree_object"]

    data = {
        "trees1": [data_tree, data_tree],
        "trees2": data_tree,
        "trees3": "data_tree",
    }

    result = deserialize_struct(DataClassWithTrees, data)

    assert isinstance(result, DataClassWithTrees)
    assert isinstance(result.trees1, list)
    assert len(result.trees1) == 2
    assert all(isinstance(tree, ExampleTree) for tree in result.trees1)
    print(RenderTree(result.trees1[0]))
    assert len(result.trees1[0].descendants) == 3
    assert isinstance(result.trees2, ExampleTree)
    assert isinstance(result.trees3, str)

    # Check serialization and compare with standard `asdict`
    dict_data_good = serialize_struct(result)
    dict_data_bad = attr.asdict(result)

    assert isinstance(dict_data_good["trees1"][0], dict)
    assert isinstance(dict_data_good["trees2"], dict)
    assert isinstance(dict_data_good["trees3"], str)
    assert dict_data_good == testing_set["example_nested_tree_objects"]

    assert isinstance(dict_data_bad["trees1"], list)
    assert isinstance(dict_data_bad["trees1"][0], ExampleTree)
    assert isinstance(dict_data_bad["trees2"], ExampleTree)
    assert isinstance(dict_data_bad["trees3"], str)

    # Check JSON compatibility
    assert json.loads(json.dumps(dict_data_good))
    with pytest.raises(TypeError):
        assert json.loads(json.dumps(dict_data_bad))
